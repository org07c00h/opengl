#pragma once
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.hpp"
#include "Texture.hpp"
class SpriteRenderer
{
public:
    SpriteRenderer(const Shader& t_shader);
    ~SpriteRenderer();

    void DrawSprite(const Texture2D &t_texture, glm::vec2 position, 
        glm::vec2 size = glm::vec2(10, 10), GLfloat rotate = 0.0f, 
        glm::vec3 color = glm::vec3(1.0f));
private:
    Shader m_shader;
    GLuint m_quadVAO;

    void initRenderData();
};