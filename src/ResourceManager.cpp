#include <iostream>
#include <sstream>
#include <fstream>
#include <SOIL/SOIL.h>
#include "ResourceManager.hpp"
#include "GameException.hpp"
// Instantiate static variables
std::map<std::string, Texture2D> ResourceManager::Textures;
std::map<std::string, Shader> ResourceManager::Shaders;
std::map<GLchar, Character> ResourceManager::Characters;

Shader& ResourceManager::LoadShader(const GLchar *vShaderFile, 
    const GLchar *fShaderFile, const GLchar *gShaderFile, std::string name)
{
    Shaders[name] = loadShaderFromFile(vShaderFile, fShaderFile, gShaderFile);
    return Shaders[name];
}

Shader& ResourceManager::GetShader(const std::string& name)
{
    return Shaders[name];
}

Texture2D& ResourceManager::LoadTexture(const GLchar *file, GLboolean alpha, 
    std::string name)
{
    Textures[name] = loadTextureFromFile(file, alpha);
    return Textures[name];
}

Texture2D& ResourceManager::GetTexture(const std::string& name)
{
    return Textures[name];
}

void ResourceManager::Clear()
{
    // (Properly) delete all shaders	
    for (auto iter : Shaders)
        glDeleteProgram(iter.second.ID);
    // (Properly) delete all textures
    for (auto iter : Textures)
        glDeleteTextures(1, &iter.second.ID);
}

Shader ResourceManager::loadShaderFromFile(const GLchar *vShaderFile, 
    const GLchar *fShaderFile, const GLchar *gShaderFile)
{
    // 1. Retrieve the vertex/fragment source code from filePath
    std::string vertexCode;
    std::string fragmentCode;
    std::string geometryCode;
    try
    {
        // Open files
        std::ifstream vertexShaderFile(vShaderFile);
        std::ifstream fragmentShaderFile(fShaderFile);
        std::stringstream vShaderStream, fShaderStream;
        // Read file's buffer contents into streams
        vShaderStream << vertexShaderFile.rdbuf();
        fShaderStream << fragmentShaderFile.rdbuf();
        // close file handlers
        vertexShaderFile.close();
        fragmentShaderFile.close();
        // Convert stream into string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
        // If geometry shader path is present, also load a geometry shader
        if (gShaderFile != nullptr)
        {
            std::ifstream geometryShaderFile(gShaderFile);
            std::stringstream gShaderStream;
            gShaderStream << geometryShaderFile.rdbuf();
            geometryShaderFile.close();
            geometryCode = gShaderStream.str();
        }
    }
    catch (std::exception e)
    {
        std::cout << "ERROR::SHADER: Failed to read shader files" << std::endl;
    }
    const GLchar *vShaderCode = vertexCode.c_str();
    const GLchar *fShaderCode = fragmentCode.c_str();
    const GLchar *gShaderCode = geometryCode.c_str();
    // 2. Now create shader object from source code
    Shader shader;
    shader.Compile(vShaderCode, fShaderCode, 
        gShaderFile != nullptr ? gShaderCode : nullptr);
    return shader;
}

Texture2D ResourceManager::loadTextureFromFile(const GLchar *file,
    GLboolean alpha)
{
    // Create Texture object
    Texture2D texture;
    if (alpha)
    {
        texture.m_internalFormat = GL_RGBA;
        texture.m_imageFormat = GL_RGBA;
    }
    // Load image
    int width, height;
    unsigned char* image = SOIL_load_image(file, &width, &height, 0, 
        texture.m_imageFormat == GL_RGBA ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB);
    if (image == 0)
    {
        std::string msg = std::string("Failed to load image: ");
        msg += file;
        exceptions::throwGameError(exceptions::Error::TEXTURE_ERROR,
            msg);
    }
    // Now generate texture
    texture.Generate(width, height, image);
    // And finally free image data
    SOIL_free_image_data(image);
    return texture;
}
void ResourceManager::LoadFont(const GLchar* t_file)
{
    FT_Library m_ft;
    FT_Face m_face;
    if (FT_Init_FreeType(&m_ft))
    {
        throwGameError(exceptions::Error::FONT_ERROR,
            "Could not init FreeType Library");
    }
    if (FT_New_Face(m_ft, t_file, 0, &m_face))
    {
        std::string msg = "Failed to load the font: ";
        msg += t_file;
        throwGameError(exceptions::Error::FONT_ERROR, msg);
    }
    FT_Set_Pixel_Sizes(m_face, 0, 48);
    for (GLubyte c = 0; c < 128; c++)
    {
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); 
        Characters[(GLchar)c] = renderGlyph(c, &m_face);
    }
    FT_Done_Face(m_face);
    FT_Done_FreeType(m_ft);
}
Character ResourceManager::renderGlyph(GLubyte c, const FT_Face* t_face)
{
    FT_Face m_face = *t_face;
    if (FT_Load_Char(m_face, c, FT_LOAD_RENDER))
    {
        throwGameError(exceptions::Error::FONT_ERROR, "Failed to load Glyph");
    }
    Texture2D texture;
    texture.m_internalFormat = GL_RED;
    texture.m_imageFormat = GL_RED;
    texture.m_wrapS =  GL_CLAMP_TO_EDGE;
    texture.m_wrapT =  GL_CLAMP_TO_EDGE;
    texture.m_filterMin = GL_LINEAR;
    texture.m_filterMax = GL_LINEAR;
    texture.Generate(m_face->glyph->bitmap.width, m_face->glyph->bitmap.rows,
        m_face->glyph->bitmap.buffer);
    std::string name = "font_" + (char)c;
    Textures[name] = texture;
    Character character = {
        name,
        glm::ivec2(m_face->glyph->bitmap.width, m_face->glyph->bitmap.rows),
        glm::ivec2(m_face->glyph->bitmap_left, m_face->glyph->bitmap_top),
        m_face->glyph->advance.x
    };
    return character;
}
Character ResourceManager::GetCharacter(const GLchar& c)
{
    return Characters[c];
}