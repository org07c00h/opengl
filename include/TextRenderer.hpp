#pragma once
#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "ResourceManager.hpp"
#include "SpriteRenderer.hpp"
class TextRenderer
{
public:
	TextRenderer(const std::string& t_text, const Shader& t_shader);
	~TextRenderer();
	void DrawText(glm::vec2 position, glm::vec2 size, glm::vec3 color);
private:
	void initRenderData();
	Shader m_shader;
	std::string m_text;
	SpriteRenderer* m_render;
};