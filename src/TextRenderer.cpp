#include "TextRenderer.hpp"
TextRenderer::TextRenderer(const std::string& t_text, const Shader& t_shader)
{
	m_text = t_text;
	m_shader = t_shader;
	initRenderData();
}
TextRenderer::~TextRenderer()
{
///	glDeleteVertexArrays(1, &m_quadVAO);
}
void TextRenderer::initRenderData()
{
	m_render = new SpriteRenderer(ResourceManager::GetShader("font"));
}
void TextRenderer::DrawText(glm::vec2 position, glm::vec2 size, glm::vec3 color)
{
	glm::vec2 newPosition(position);
	for(auto c = m_text.begin(); c != m_text.end(); c++)
	{
		Character ch = ResourceManager::GetCharacter(*c);
		newPosition.x = position.x + rand() % 2;
	    newPosition.y = position.y + (ResourceManager::GetCharacter('H').Bearing.y - ch.Bearing.y) + rand() % 2; 

		m_render->DrawSprite(ResourceManager::GetTexture(ch.TextureName), newPosition,
			ch.Size, 0.0f, color);    
		position.x += (ch.Advance >> 6);
	}
}