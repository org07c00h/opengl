#pragma once
#include <GL/glew.h>
class ResourceManager;
// Texture2D is able to store and configure a texture in OpenGL.
// It also hosts utility functions for easy management.
class Texture2D
{
public:
    // Holds the ID of the texture object, used for all texture operations to reference to this particlar texture
    GLuint ID;
    // Constructor (sets default texture modes)
    Texture2D();
    // Generates texture from image data
    void Generate(GLuint t_width, GLuint t_height, unsigned char* t_data);
    // Binds the texture as the current active GL_TEXTURE_2D texture object
    void Bind() const;
private:
    GLuint m_width {0};
    GLuint m_height {0};
    GLuint m_internalFormat {GL_RGB}; // Format of texture object
    GLuint m_imageFormat {GL_RGB}; // Format of loaded image
    // Texture configuration
    GLuint m_wrapS {GL_REPEAT}; // Wrapping mode on S axis
    GLuint m_wrapT {GL_REPEAT}; // Wrapping mode on T axis
    GLuint m_filterMin {GL_LINEAR}; // Filtering mode if texture pixels < screen pixels
    GLuint m_filterMax {GL_LINEAR}; // Filtering mode if texture pixels > screen pixels

    friend class ResourceManager;
};