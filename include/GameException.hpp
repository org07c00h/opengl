#pragma once
#include <stdexcept>
#include <string>
struct GameException : public std::runtime_error
	{
		GameException(const char* msg) : runtime_error(msg) {}
	};
namespace exceptions
{
	enum Error
	{
		SHADER_ERROR,
		TEXTURE_ERROR,
		FONT_ERROR
	};	
	class shader: public GameException {
		using GameException::GameException;
	};
	class texture2d: public GameException {
		using GameException::GameException;
	};
	class font: public GameException {
		using GameException::GameException;
	};
	static void throwGameError(const int& error_code, const char* msg)
	{
		switch (error_code)
		{
			case Error::SHADER_ERROR:
				throw exceptions::shader(msg);
			break;
			case Error::TEXTURE_ERROR:
				throw exceptions::texture2d(msg);
			case Error::FONT_ERROR:
				throw exceptions::font(msg);
			break;
			default:
				throw GameException(msg);
			break;
		}
	}
	static void throwGameError(const int& error_code, const std::string& msg)
	{
		throwGameError(error_code, msg.c_str());
	}
}