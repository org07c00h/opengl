#include "Game.hpp"
#include "SpriteRenderer.hpp"
#include "ResourceManager.hpp"
#include "Shader.hpp"
#include "GameException.hpp"
#include "TextRenderer.hpp"
#include <iostream>
SpriteRenderer  *Renderer;
TextRenderer *TextR;
Game::Game(GLuint width, GLuint height) :
	State(GAME_ACTIVE),
	Keys(),
	Width(width),
	Height(height)
{
	
}
Game::~Game()
{

}
void Game::Init()
{
	// Load shaders
    try
    {
        ResourceManager::LoadShader("./assets/shaders/sprite.vs",
    	   "./assets/shaders/sprite.frag", nullptr, "sprite");
        ResourceManager::LoadShader("./assets/shaders/font.vs",
           "./assets/shaders/font.frag", nullptr, "font");
    }
    catch (GameException& e)
    {
        std::cout << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    // Configure shaders
    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(this->Width),
    	static_cast<GLfloat>(this->Height), 0.0f, -1.0f, 1.0f);
    ResourceManager::GetShader("sprite").Use().SetInteger("image", 0);
    ResourceManager::GetShader("sprite").SetMatrix4("projection", projection);
    ResourceManager::GetShader("font").Use().SetInteger("image", 0);
    ResourceManager::GetShader("font").SetMatrix4("projection", projection);
    // Load textures
    try {
        ResourceManager::LoadTexture("./assets/images/something.png", GL_TRUE,
    	   "face");
        ResourceManager::LoadFont("./assets/fonts/Roboto-Regular.ttf");
    }
    catch (GameException& e)
    {
        std::cout << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    // Set render-specific controls
    Renderer = new SpriteRenderer(ResourceManager::GetShader("sprite"));
    TextR = new TextRenderer("This is a sample text", ResourceManager::GetShader("font"));
}
void Game::Update(GLfloat dt)
{

}
void Game::ProcessInput(GLfloat dt)
{

}
void Game::Render()
{
    Renderer->DrawSprite(ResourceManager::GetTexture("face"), 
    	glm::vec2(200, 200), glm::vec2(300, 400), 45.0f, 
    	glm::vec3(0.0f, 1.0f, 0.0f));
    TextR->DrawText(glm::vec2(10, 100), glm::vec2(20, 20), glm::vec3(1.0f, 1.0f, 1.0f));
}